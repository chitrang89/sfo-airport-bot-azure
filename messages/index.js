"use strict";
var botbuilder_azure = require("botbuilder-azure");



/**
 * Created by chitrang89 on 23/03/17.
 */
var restify = require('restify');
var builder = require('botbuilder');
var http = require('request');
var Q = require ('q');
var rp = require('request-promise');
var responseBuilder = require('./ResponseBuilder').ResponseBuilder;
var bodyParser=require("body-parser");
var _=require("underscore");
var striptags = require('html-parse-stringify');
var moment_tz = require('moment-timezone');
var azure = require('azure');
// var serviceBusService = azure.createServiceBusService("Endpoint=sb://airportbot.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=uMTxlxIE98xp/wcPq2Q/43igmRnYMuwojkVHFqY4Fzw=");
var addr=null;


// serviceBusService.receiveQueueMessage('subscribeforflightupdates', function(error, receivedMessage){
//     console.log('received');
//     if(!error){
//         var msg = new builder.Message()
//             .address(addr)
//             .text(receivedMessage);
//         bot.send(msg, function (err) {
//             // Return success/failure
//             console.log('err->'+error)
//         });    }
// });
var useEmulator = (process.env.NODE_ENV === 'development');
var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
    appId: process.env['MicrosoftAppId'],
    appPassword: process.env['MicrosoftAppPassword'],
    stateEndpoint: process.env['BotStateEndpoint'],
    openIdMetadata: process.env['BotOpenIdMetadata']
});
var epoch = moment_tz().tz("America/Los_Angeles").valueOf();
var bot = new builder.UniversalBot(connector);
var server = restify.createServer();
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
bot.dialog('reset', [function (session){
    session.userData = {};
    console.log(session.userData.selectedFlight);
    session.replaceDialog('/');
}])
    .triggerAction({
        matches: [/reset/i, /cancel/i, /return/i, /start again/i, /nevermind/i]
    });

bot.dialog('help', [function (session){
    session.send('Start by saying hello or simply type search to start searching for flights');
}])
    .triggerAction({
        matches: [/lost/i, /help/i, /can you help me/i, /dont know/i, /lost/i]
    });
bot.dialog('searchFromHelp', [function (session){
session.replaceDialog('/typeOfFlight');
}])
    .triggerAction({
        matches: [/search/i]
    });


bot.dialog('thanks', [function (session){
    if(session.userData.name === undefined)
        getUserNameFirst(session);
    else
        session.send('Always welcome %s!',session.userData.name);
//  session.beginDialog('/typeOfFlight');
}])
    .triggerAction({
        matches: [/thanks/i, /thank you/i, /cheers/i, /awesome/i, /great/i, /wonderful/i,/amazing/i]
    });

function getUserNameFirst(session)
{
    session.replaceDialog('/getNameAgain');
}

bot.dialog('/getNameAgain',[function(session,args){
            builder.Prompts.text(session, 'Oops didn\'t get your name there?');
},function(session,results){
        session.userData.name = results.response;
        session.send('Hello %s!', results.response);
        session.replaceDialog('/typeOfFlight');
}]);
bot.use(builder.Middleware.sendTyping());

if (useEmulator) {
    server.listen(3978, function() {
        console.log('test bot endpont at http://localhost:3978/api/messages');
    });
    server.post('/api/messages', connector.listen());    
} else {
    module.exports = { default: connector.listen() }
}

bot.dialog('/', [
    function (session) {
        session.userData.flightToSubscribe=[]; //instantiate subscription collection
    // console.log('name found->'+session.userData.name);
        if (session.userData.name === undefined || session.userData.name === 'User' ||session.userData.name === null) {
            // do something
            session.beginDialog('/askName'); 
        }


    else {
        session.beginDialog('/typeOfFlight');

    }
    },
    function (session, results) {
        session.userData.name = results.response.entity;
    }
]);
bot.beginDialogAction('subscribe', '/subscribe');
bot.beginDialogAction('delete', '/delete');

bot.dialog('/returningUser',[function(session){
    session.send("Welcome back %s!",session.userData.name);
},function(session,results){}])

bot.dialog('/typeOfFlight', [
    function (session) {
        builder.Prompts.choice(session, "Select type of flight or enter your flight number to get started", ["Arrival","Departures"],{maxRetries:0});
    },
    function (session, results) {
        if(results.response===undefined)
        {
            session.userData.flightToSearch = session.message.text;
            session.send('Okay %s let me check that flight for you..',session.userData.name);
            session.sendTyping();
            session.replaceDialog('/searchFlight')
        }
        if(results.response!==undefined)
        {
            switch (results.response.entity) {
            case "Arrival":
                session.userData.flightType="A";
                break;
            case "Departures":
                session.userData.flightType="D";
                break;
        }
        session.replaceDialog('/selectAirlines');
        }
        
    }
]);

bot.dialog('/selectAirlines',[
    function(session){
        responseBuilder.prototype.selectAirlines(builder,session,_,rp,epoch);

    },
    function(session,results)
    {
        session.userData.selectedAirport = results.response.entity;
        session.replaceDialog('/location');
    }
]);


bot.dialog('/selectFlight',[
    function(session) {
        responseBuilder.prototype.getFlights(builder,session,_,rp,epoch,moment_tz);

    },
    function(session,results)
    {
        session.userData.selectedFlight = results.response.entity;
        
    }
]);

bot.dialog('/searchFlight',[
    function(session) {
        responseBuilder.prototype.searchFlights(builder,session,_,rp,epoch,moment_tz);

    },
    function(session,results)
    {
        session.userData.selectedFlight = results.response.entity;
    }
]);


bot.dialog('/location',[
    function(session) {
        responseBuilder.prototype.getLocation(builder,session,_,rp,epoch,moment_tz);

    },
    function(session,results)
    {
        session.userData.selectedLocation = results.response.entity;
        session.replaceDialog('/selectFlight');

    }
]);

bot.dialog('/delete',[
    function(session,args){
        // var flightToSave = {
        //     flight:{},
        //     hasSubscribed:true,
        //     isActive:false,
        // };
        // flightToSave.flight = args.data;
        // session.userData.flightToSubscribe.push(flightToSave);
        // responseBuilder.prototype.subscribe(session,serviceBusService,_,false);
        session.send('Turning off updates now...')
        //send subscription message along with convo address and flight data
    },function(session,results){
        ///respond with error
    }
])

bot.dialog('/subscribe',[
    function(session,args){
    //     var flightToSave = {
    //         flight:{},
    //         hasSubscribed:false,
    //         isActive:true,

    //     };
    //     var f = args.data;
    //     var remark=null;
    //     if(!_.isEmpty(f.Remark))
    //                 remark = f.Remark;
    //             else
    //                 remark = "On Time"

    //     flightToSave.flight = {
    //         FlightNumber:f.FlightNumFull,
    //         City:f.CityFull,
    //         Terminal:f.Terminal,
    //         Status:remark,
    //         Gate:f.Gate,
    //         EstimatedTime:moment_tz(f.EstTimeMili, ["HH"]).format("hh A"),
    //         ScheduledTime:f.SchedTime,
    //         FlightType:f.FlightType,
    //     };
    //     if(session.userData.flightToSubscribe===undefined)
    //         session.userData.flightToSubscribe=[];

    //     session.userData.flightToSubscribe.push(flightToSave);
    //     responseBuilder.prototype.subscribe(session,serviceBusService,_,true);
        session.send('subscribing you now...')
        //send subscription message along with convo address and flight data
    },function(session,results){
        ///respond with error
    }
])

bot.dialog('/askName', [
    function (session) {
    if(session.message.user.name===undefined || session.message.user.name==='User' ||session.userData.name==undefined && req.body.address.user.name===undefined)
        builder.Prompts.text(session, 'Hi! What is your name?');
    else
    {
        session.userData.name = req.body.address.user.name;
        session.send('Hello %s!',req.body.address.user.name);
        session.replaceDialog('/typeOfFlight');
    }

    },
    function (session, results) {
        session.userData.name = results.response;
        session.send('Hello %s!', results.response);
        session.replaceDialog('/typeOfFlight');
    }
]);

server.post('/api/notify', function (req, res) {
    console.log('req->'+req);
    console.log('res->'+res);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    // Process posted notification
    var address = JSON.parse(req.body.address);
    var notification = req.body.notification;

    // Send notification as a proactive message
    var msg = new builder.Message()
        .address(address)
        .text(notification);
    bot.send(msg, function (err) {
        // Return success/failure
        res.status(err ? 500 : 200);
        res.end();
    });
});

