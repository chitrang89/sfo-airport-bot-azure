/**
 * Created by chitrang89 on 23/03/17.
 */
'use strict'
function ResponseBuilder(){


}
ResponseBuilder.prototype.subscribe = function(session,serviceBusService,_,isCreate){
    return notify(session,serviceBusService,_,isCreate);
}

ResponseBuilder.prototype.selectAirlines = function(builder,session,_,rp,epoch){
    return selectAirlines(builder,session,_,rp,epoch);
}
ResponseBuilder.prototype.getFlights = function(builder,session,_,rp,epoch,moment){
    return getFlights(builder,session,_,rp,epoch,moment);
}

ResponseBuilder.prototype.searchFlights = function(builder,session,_,rp,epoch,moment){
    return searchFlights(builder,session,_,rp,epoch,moment);
}
ResponseBuilder.prototype.getLocation = function(builder,session,_,rp,epoch,moment){
    return getLocation(builder,session,_,rp,epoch,moment);
}

function alphabetical(a, b)
{
    var A = a.toLowerCase();
    var B = b.toLowerCase();
    if (A < B){
        return -1;
    }else if (A > B){
        return  1;
    }else{
        return 0;
    }
}

var subscriptionModel = {
    address:{},
    flight:{},
    isActive:true,
    id:guid(),
    hasSubscribed:false
}

function notify(session,serviceBusService,_,isCreate){
     var address = session.message.address;
     subscriptionModel.address = address;
     var flightToUpdateWhenSubscribed = {};
     _.each(session.userData.flightToSubscribe,function(item){
         if(!item.hasSubscribed) //new subscription
         {
             subscriptionModel.flight = item;
             flightToUpdateWhenSubscribed = item; //get local copy to update later
         }
     });

     if(subscriptionModel.flight.isActive)
     {
             // Save subscription with address to storage.
            serviceBusService.sendQueueMessage('subscribeforflightupdates', {body:JSON.stringify(subscriptionModel)}, function(error){
                if(!error){
                    if(isCreate)
                        session.send('Great! I shall send you flight updates as we go!');
                    else
                        session.send("Successfully unsubscribed.")

                    _.each(session.userData.flightToSubscribe,function(item){
                        if(item = flightToUpdateWhenSubscribed)
                            item.hasSubscribed=true;
                        if(!isCreate)
                            item.isActive=false; 
                    })
                }
                console.log('err->'+error)
            
            
            });
     }
           
}

function selectAirlines(builder,session,_,rp,epoch){
    getAirlines(rp,_,epoch,builder,session);
}

function getAirlines(rp,_,epoch,builder,session){
    rp('https://www.flysfo.com/flightprocessing/fullFlightData.txt?_='+epoch)
        .then(function (response) {
            var flights=[];
           var flightType=session.userData.flightType;

            var d = JSON.parse(response);

            _.each(d.aaData,function(item){
                if(item.FlightType==flightType)
                    flights.push(item.CarrierFull);
            });
            var k = _.uniq(flights);
            var groupedContacts = k.sort(alphabetical);

            builder.Prompts.choice(session, "Airlines:", groupedContacts);
            session.send("Select Airline");
            return;
        }).catch(function(err){
            console.log('couldnt get->'+err)
        session.send('Sorry, No results found.')
        session.endDialog();
        // return [];
        })
}
function getLocation(builder,session,_,rp,epoch,moment0){
    rp('https://www.flysfo.com/flightprocessing/fullFlightData.txt?_='+epoch)
        .then(function (response) {
            var flights=[];
            var d = JSON.parse(response);
            var selectedAirport=session.userData.selectedAirport;
            var flightType=session.userData.flightType;
            _.each(d.aaData,function(item){

                if(flightType==="D" && item.CarrierFull===selectedAirport)
                {
                    flights.push(item.CityFull);

                }
                 if(flightType==="A"&& item.CarrierFull===selectedAirport)
                {
                    flights.push(item.CityFull);

                }

            });
            var k = _.uniq(flights);
            var groupedContacts = k.sort(alphabetical);

            builder.Prompts.choice(session, "Locations:", groupedContacts);
            session.send("Select Location");
            return;
        }).catch(function(err){
        console.log('couldnt get->'+err)
        session.send('Sorry, No results found.')
        session.endDialog();
        // return [];
        })
}

function getFlights(builder,session,_,rp,epoch,moment){

    var welcomeCard = new builder.Message(session)
        .textFormat(builder.TextFormat.xml);
        rp('https://www.flysfo.com/flightprocessing/fullFlightData.txt?_='+epoch)
        .then(function (response) {

            var selectedAirport=session.userData.selectedAirport;
            var flightType=session.userData.flightType;
            var location=session.userData.selectedLocation;
            var flights=[];
            var d = JSON.parse(response);
            
            _.each(d.aaData,function(item){
                var remark = '';
                if(!_.isEmpty(item.Remark))
                    remark = item.Remark;
                else
                    remark = "On Time"

                if(item.CarrierFull===selectedAirport && item.FlightType==flightType)
                {
                    if(flightType==="D" && item.CityFull===location)
                    {
                        flights.push(
                                new builder.HeroCard(session)
                                    .title(item.FlightNumFull+" To "+item.CityFull)
                                    .subtitle("Terminal: "+item.Terminal+" Gate:"+item.Gate+" Status: "+remark )
                                    .text("Scheduled:"+item.SchedTime+ " Estimated: "+ moment(item.EstTimeMili, ["HH"]).format("hh A"))
                                .buttons([builder.CardAction.dialogAction(session,'subscribe',item,'Subscribe'),builder.CardAction.dialogAction(session,'delete',item,'Delete')])
);

                    }
                    else if(flightType==="A" && item.CityFull===location)
                    {

                        flights.push(
                            new builder.HeroCard(session)
                                .title(item.FlightNumFull+" From "+item.CityFull)
                                .subtitle("Terminal: "+item.Terminal+" Gate:"+item.Gate+" Status: "+remark)
                                .text("Scheduled:"+item.SchedTime+ " Estimated: "+ moment(item.EstTimeMili, ["HH"]).format("hh A"))
                                .buttons([builder.CardAction.dialogAction(session,'subscribe',item,'Subscribe'),builder.CardAction.dialogAction(session,'delete',item,'Delete')])

                                
                        );

                    }


                }
            });
            if(flights.length<1)
                flights.push(new builder.HeroCard(session).title('Sorry, no results found.'));

            welcomeCard.attachments(flights)

            session.endDialog(welcomeCard);

        })
        .catch(function (err) {
            console.log('couldnt get->'+err)

            var e = [];
            e.push(new builder.HeroCard(session).title("An error occurred. Please try again or contact the developer."));
            welcomeCard.attachments(e);
            session.send(welcomeCard);
            session.endDialog();
            // Crawling failed...
        });

}


function searchFlights(builder,session,_,rp,epoch,moment){

    var welcomeCard = new builder.Message(session)
        .textFormat(builder.TextFormat.xml);
        rp('https://www.flysfo.com/flightprocessing/fullFlightData.txt?_='+epoch)
        .then(function (response) {

            var f = session.userData.flightToSearch;
            var flights=[];
            var d = JSON.parse(response);

            _.each(d.aaData,function(item){
                var remark = '';
                if(!_.isEmpty(item.Remark))
                    remark = item.Remark;
                else
                    remark = "On Time"

                if(item.FlightNumFull.toLowerCase()===f)
                {
                    if(item.FlightType==="D")
                    {
                        flights.push(
                                new builder.HeroCard(session)
                                    .title(item.FlightNumFull+" To "+item.CityFull)
                                    .subtitle("Terminal: "+item.Terminal+" Gate:"+item.Gate+" Status: "+remark )
                                    .text("Scheduled:"+item.SchedTime+ " Estimated: "+ moment(item.EstTimeMili, ["HH"]).format("hh A"))
                                    .buttons([builder.CardAction.dialogAction(session,'subscribe',item,'Subscribe'),builder.CardAction.dialogAction(session,'delete',item,'Delete')])
);

                    }
                    else if(item.FlightType==="A")

                    {

                        flights.push(
                            new builder.HeroCard(session)
                                .title(item.FlightNumFull+" From "+item.CityFull)
                                .subtitle("Terminal: "+item.Terminal+" Gate:"+item.Gate+" Status: "+remark)
                                .text("Scheduled:"+item.SchedTime+ " Estimated: "+ moment(item.EstTimeMili, ["HH"]).format("hh A"))
                                .buttons([builder.CardAction.dialogAction(session,'subscribe',item,'Subscribe'),builder.CardAction.dialogAction(session,'delete',item,'Delete')])

                        );

                    }


                }
            });
            if(flights.length<1)
                flights.push(new builder.HeroCard(session).title('Sorry, no results found.'));

            welcomeCard.attachments(flights)

            session.send(welcomeCard);

        })
        .catch(function (err) {
            console.log('couldnt get->'+err)

            var e = [];
            e.push(new builder.HeroCard(session).title("An error occurred. Please try again or contact the developer."));
            welcomeCard.attachments(e);

            session.endDialog(welcomeCard);
            // Crawling failed...
        });

}

function save(selectedAirport,session){
    session.userData.selectedAirport=selectedAirport;
}
/*Guid Helper */
function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}
/*end guid helper */

module.exports.ResponseBuilder = ResponseBuilder;
